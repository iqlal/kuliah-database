```mermaid

---
title: Informatics Library
---

erDiagram
    Library{

    }

    Book {
        int book_id
        string book_title
        string book_author
    }

    Publisher {
        int publisher_id
        string publisher_name
        string publisher_address
    }

    Member {
        int member_id
        string member_name
        string member_address
        int member_phone
    }

    Librarian {
        int librarian_id
        string librarian_name
        int librarian_phone
    }

    Borrowed {
        int issue_id
        int loan_date
        int return_date
    }

    %% Relational

    Library ||--|{ Book : has
    Library ||--|{ Member : has
    Library ||--|{ Librarian : has

    Librarian |{--}| Book : manages

    Book |{--}| Publisher : published

```